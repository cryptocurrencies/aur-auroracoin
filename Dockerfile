# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libevent-dev libboost-all-dev libdb-dev libdb++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/aurarad/Auroracoin.git /opt/auroracoin
RUN cd /opt/auroracoin/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/auroracoin/ && \
    make

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb-dev libdb++-dev libevent-dev libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r auroracoin && useradd -r -m -g auroracoin auroracoin
RUN mkdir /data
RUN chown auroracoin:auroracoin /data
COPY --from=build /opt/auroracoin/src/auroracoind /usr/local/bin/auroracoind
COPY --from=build /opt/auroracoin/src/auroracoin-cli /usr/local/bin/auroracoin-cli
USER auroracoin
VOLUME /data
EXPOSE 9402 9401
CMD ["/usr/local/bin/auroracoind", "-datadir=/data", "-conf=/data/auroracoin.conf", "-server", "-txindex", "-printtoconsole"]