This Dockerfile generates the latest Auroracoind client used in server applications. It is anticipated that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name auroracoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/aur-auroracoin:latest
```